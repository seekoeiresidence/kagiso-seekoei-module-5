import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
        apiKey: "AIzaSyCEUHKkMcUqT1oYbJAyZjZyJqQD8lzxPpM",
        authDomain: "mtnacademyweb.firebaseapp.com",
        projectId: "mtnacademyweb",
        storageBucket: "mtnacademyweb.appspot.com",
        messagingSenderId: "186888892615",
        appId: "1:186888892615:web:09395ba04c031b6e044ee2")
  ); 

  runApp(MaterialApp(
    theme: ThemeData(
       colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.orange,
       ).copyWith(secondary: Colors.orange[300],)
    ),

    title: 'Module 5',
    home: FirstRoute(),
  ));
}

class FirstRoute extends StatelessWidget {
  const FirstRoute({super.key});

  @override
  Widget build(BuildContext context) {
    /***TextField Variables */
    TextEditingController carNameController = TextEditingController();
    TextEditingController carModelController = TextEditingController();
    TextEditingController licenseController = TextEditingController();

    Future _addCars() {
       final carname = carNameController.text;
       final carmodel = carModelController.text;
       final licensenr = licenseController.text;
       final ref=  FirebaseFirestore.instance.collection("carsInventory").doc();

       return ref .set({"carName": carname, "carModel": carmodel, "licenseNr": licensenr, "doc_id": ref.id})
                  .then((value) => log("Successful entry!!"))
                  .catchError((onError)=> log(onError));
            
    }
    /***end */
    final style = ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));
    return Scaffold(
      appBar: AppBar(
        title: const Text('Car Inventory', style: TextStyle(color: Colors.white,), ),
      ),
body: Column(
  children: [
        Column(
    
    crossAxisAlignment: CrossAxisAlignment.center,
    
             mainAxisAlignment: MainAxisAlignment.start,
    
             children: [ 
    
    
    
               const SizedBox(height: 2),
    
               Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
    
               child: TextFormField(
    
               controller: carNameController,
    
               decoration: const InputDecoration(
    
                  border: UnderlineInputBorder(),
    
                  labelText: 'Enter car name',
    
               ),
    
               ),
    
               ),
    
               const SizedBox(height: 5),
    
               Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
    
               child: TextFormField(
    
               controller: carModelController,
    
               decoration: const InputDecoration(
    
                  border: UnderlineInputBorder(),
    
                  labelText: 'Enter car model',
    
               ),
    
               ),
    
               ),
    
               const SizedBox(height: 5),
    
               Padding(padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
    
               child: TextFormField(   
    
               controller: licenseController,
    
               decoration: const InputDecoration(
    
                  border: UnderlineInputBorder(),
    
                  labelText: 'Enter license nr.',
    
               ),
    
               ),
    
               ),
    
               const SizedBox(height: 20),
    
               ElevatedButton(
    
                  style: style,
    
                         onPressed: () {
    
                           _addCars();
    
                         },
    
                         child: const Text('Create Entry', style: TextStyle(color: Colors.white,),),
    
               )
    
             ],                                                                   
    
           ),
           InventoryList()
  ],
)
      );

  }
}


class InventoryList extends StatelessWidget {
   InventoryList({super.key});
  final Stream<QuerySnapshot> _mySessions = FirebaseFirestore.instance.collection("carsInventory").snapshots();

  @override
  Widget build(BuildContext context) {
    final style = ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));
    TextEditingController _carNameUpdateHint = TextEditingController();
    TextEditingController _carModelUpdateHint = TextEditingController();
    TextEditingController _licenseNrUpdateHint = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
                      .collection("carsInventory").doc(docId)
                      .delete().then((value) => print("Entry removed"));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("carsInventory");
      _carNameUpdateHint.text = data["carName"];
      _carModelUpdateHint.text = data["carModel"];
      _licenseNrUpdateHint.text = data["licenseNr"];

      showDialog(context: context,
                 builder: (_) => AlertDialog(
                    title: Text("Update Entry"),
                    content: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextField(controller: _carNameUpdateHint,),
                        TextField(controller: _carModelUpdateHint,),
                        TextField(controller: _licenseNrUpdateHint,),
                        TextButton(onPressed: () {
                            collection.doc(data["doc_id"])
                            .update({
                              "carName": _carNameUpdateHint.text,
                              "carModel": _carModelUpdateHint,
                              "licenseNr": _licenseNrUpdateHint
                            });
                            Navigator.pop(context);  
                        },
                                   child: Text("Done"))
                      ],
                    ),
                 ));
    }

    return StreamBuilder(
          stream: _mySessions,
          builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
              if(snapshot.hasError) {
                return const Text("Something went wrong");
              }
              if(snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              }
              if(snapshot.hasData) {
                
                return Row(
                  children: [
                    Expanded(child: SizedBox(
                      height: (MediaQuery.of(context).size.height),
                      width: MediaQuery.of(context).size.width,

                      child: ListView(
                        children: snapshot.data!.docs
                             .map((DocumentSnapshot documentSnapshot) {
                                 Map<String, dynamic> data = documentSnapshot.data()! as Map<String, dynamic>;
                                
                                 return Column(
                                    children: [
                                      Card(
                                        child: Column(children: [
                                           ListTile(
                                             leading: Text(data["carName"]),
                                             title: Text(data['carModel']),
                                             subtitle: Text(data['licenseNr']),
                                           ),
                                            ButtonTheme(
                              child: ButtonBar(
                                children: [
                                   OutlinedButton.icon(
                                    onPressed: () {_update(data);}, 
                                    icon: Icon(Icons.pen),
                                    label: Text('Edit'),
                                   ),
                                   OutlinedButton.icon(
                                       onPressed: () {_delete(data["doc_id"]);},
                                       icon: Icon(Icons.delete),
                                       label: Text('Remove'),
                                   ),
                                ],
                              )
                            )
                                        ]),
                                      ),
                                    ],
                                 );
                             }).toList(),
                      ),))
                  ],
                );  

              }  else {
                return (Text("Inventory Empty"));
              }
          },


      );
  }
}
